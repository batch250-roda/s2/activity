package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class PrimeNumber {
    public static void main(String[] args){
        int[] primeArr = {2, 3, 5, 7, 11};

        Scanner input = new Scanner(System.in);

        System.out.print("Enter index to print the specific prime number: ");
        int num = input.nextInt();

        switch (num){
            case 1:
                System.out.println("The first prime number is: " + primeArr[num-1]);
                break;
            case 2:
                System.out.println("The second prime number is: " + primeArr[num-1]);
                break;
            case 3:
                System.out.println("The third prime number is: " + primeArr[num-1]);
                break;
            case 4:
                System.out.println("The fourth prime number is: " + primeArr[num-1]);
                break;
            case 5:
                System.out.println("The fifth prime number is: " + primeArr[num-1]);
                break;
            default:
                System.out.println("Invalid input. Please select from 1 - 5");
        }

        ArrayList<String> myFriends = new ArrayList<>(Arrays.asList("John", "Sherwin", "Jonathan", "Shylock"));

        System.out.println("My friends are: " + myFriends);

        HashMap<String, Integer> products= new HashMap<String, Integer>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of : " + products);
    }
}
